$(document).ready(function () {
    $('.work').hover(function () {
        $(this).children('.work-description').animate({
            opacity: "1"
        }, 'fast');
        $(this).children('.work-image').animate({
            opacity: "0.25"
        }, 'fast');
    }, function () {
        $(this).children('.work-description').animate({
            opacity: "0"
        }, 'fast');
        $(this).children('.work-image').animate({
            opacity: "1"
        }, 'fast');
    });
});